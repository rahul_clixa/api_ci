      <div>
        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="width:100%;background-color:#f4f6f6">
            <tbody>
               <tr>
                  <td>
                     <table align="center" valign="top" border="0" cellpadding="0" cellspacing="0" width="600" style="width:600px;background:#fff;border:1px solid #dddddd;border-collapse:collapse">
                        <tbody>
                           <tr style="text-align:center;padding:10px">
                              <td>
                                 <a href="<?php echo base_url(); ?>">
                                 <img height="55" border="0" src="<?php echo base_url(); ?>assets/frontend/logo.png" alt="Bright Tutee" style="padding:20px;vertical-align:bottom">
                                 </a>
                              </td>
                           </tr>
                           <tr>
                              <td style="text-align:center"> 
                              </td>
                           </tr>
                           <tr style="margin-left: 100px;">
                              <td style="padding:30px 50px">

                                 <p style="font-family:'HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Calibri,Helvetica,Arial,sans-serif;font-size:16px;font-stretch:normal;line-height:1.68;color:#3b3e40;margin:0 0 22px;font-weight:bold">
                                    Hi {name},
                                 </p>
                                  <p style="font-family:'HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Calibri,Helvetica,Arial,sans-serif;font-size:16px;font-stretch:normal;line-height:1.68;color:#3b3e40;margin:0px;">
                                    Thank you for registering with us, at Bright Tutee.<br>
									Your Login username is <strong>{user_name}</strong> and password is <strong>{password}</strong><br /><br />
                                 </p>   
                                
                                 <p style="font-family:'HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Calibri,Helvetica,Arial,sans-serif;font-size:16px;font-stretch:normal;line-height:1.68;color:#3b3e40;margin:0 0 22px">
                                    Thanks & Regards
                                    <br>
                                    <span style="font-weight:bold">
                                    The Bright Tutee Team
                                    </span>
                                 </p>
                                 
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:30px 50px;text-align:center;background-color:#f7f7f7;border-top:1px solid #dddddd">
                                 <p style="font-family:'Helvetica Neue',Calibri,Helvetica,Arial,sans-serif;font-weight:normal;font-size:10px;color:#666"> This message was sent by
                                    <a href="<?php echo base_url(); ?>" style="font-family:'Helvetica Neue',Calibri,Helvetica,Arial,sans-serif;font-size:10px;font-weight:bold;color:#678d00;text-decoration:none" target="_blank">Bright Tutee </a>Copyright <?php echo date('Y');?> Bright Tutee All rights reserved.
                                 </p>   
                              </td>   
                           </tr>
                        </tbody>
                     </table>
                  </td>
               </tr>
            </tbody>
         </table>
      </div>

<div class="clearfix"></div><br /><br />
