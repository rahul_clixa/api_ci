<?php 

defined('BASEPATH') OR exit('No direct script access allowed');


/*
	* Every request comes here first to checking request is valid or not
*/


class CAuthMiddleware implements Luthier\MiddlewareInterface
{
    public function run($arg)
    {
		$post = json_decode(file_get_contents("php://input"));
		$user=$post->cred->user;
		$pass=$post->cred->password;
		
		$CI = get_instance(); 
		$token = $CI->input->get_request_header('Authorization'); 
		
		//validations
		$CI->form_validation->set_data(array('user'    =>  $user,'pass'    =>  $pass,'token'    =>  $token));
		$CI->form_validation->set_rules('user', 'user', 'trim|required');
		$CI->form_validation->set_rules('pass', 'pass', 'trim|required');
		$CI->form_validation->set_rules('token', 'token', 'trim|required');
		
		//return if validation fails
		if ($CI->form_validation->run() == FALSE) {
			json_response(true,$CI->lang->line('invalid_request'),array(),200); 
		}  
		if ($this->checkValidClient($user,$pass ,$token)===true){ 
			return true; 
        }else{ 
			json_response(true,$CI->lang->line('invalid_request'),array(),200); 
        }  
    }
	
	private function checkValidClient($user_id,$user_pw,$token){
		$CI = get_instance();
		$CI->load->model('Common_model');
		$clients=$CI->config->item('clients_types');
		$check_client=$CI->uri->segment(1);
		$user = $CI->Common_model->select_fields_where('api_clients','',array('user_id'=>$user_id,'api_client_type_id'=>$clients[$check_client],'token'=>$token,'is_active'=>1,'user_pw'=>md5($user_pw)),1);
		if($user){
			 /* will be used to save time base token */
			 // $CI->Common_model->update_query_array('api_clients',array('user_id'=>$user_id,'user_pw'=>md5($user_pw)),array('token'=>$token)); 
			return true;
		}else{
			return false;
		}
	}
}