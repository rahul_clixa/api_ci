<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
 * This class encapsulates all the functionalities of online test for admit card users
 */
 
class OnlineTestController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct(); 
		
		//loading models used to fetch data from DB
		$this->load->model(['Common_model','Online_test_model']);
		
		//static values
		$this->total_limit=35;//number of questions 
		$this->quiz_time=60;//minutes
	}
	
	//create quiz for the user
	public function startAdOnlineTest()
	{
		$post = json_decode(file_get_contents("php://input"));
		$body = $post->body;
		
		//validations
		$this->form_validation->set_data(array('user_id'    =>  $post->body->user_id )); 
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		
		//return if validation fails
		if ($this->form_validation->run() == FALSE) {
			json_response(true,$this->lang->line('field_missing'),array(),200); 
		} 
		
		//create quiz for the user
		if ($data_out=$this->createQuiz($body )){
			json_response(false,$this->lang->line('success'),$data_out,200);    
        }else{
			json_response(true,$this->lang->line('invalid_request'),array(),200);   
        }  
    } 
	
	//function to create quiz
	private function createQuiz($body)
	{
		$post = json_decode(file_get_contents("php://input"));
		$body = $post->body;
		
		//check user if exists
		$user = $this->Common_model->select_fields_where('users','count(1) as cnt_num',array('id'=>$body->user_id ),1);
		if(isset($user) && $user->cnt_num==1){ 
			$total_limit=$this->total_limit;//number of questions
			$each_page=6;//each page
			$start=0;//starting number
			$quiz_time=$this->quiz_time;//minutes
			
			if(isset($post->body->start)){
				$start=$post->body->start;
			}
			
			//validations
			$this->form_validation->set_data(array('start'    =>  $start )); 
			$this->form_validation->set_rules('start', 'start', "trim|numeric|greater_than[-1]|less_than[$total_limit]");
			
			//return if validation fails
			if ($this->form_validation->run() == FALSE) { 
				json_response(true,$this->lang->line('invalid_request'),array(),200); 
			}  
			
			//check if already assigned_quiz
			$ass_quiz = $this->Common_model->select_fields_where('assigned_quiz','count(1) as cnt_num',array('users_id'=>$body->user_id ),1);
			if(isset($ass_quiz) && $ass_quiz->cnt_num>0){ 
				return false;
			}
			
			//get random questions and insert in table 
			$ins_random = $this->Common_model->runRaw("Insert into assigned_quiz SELECT '',id,'".$body->user_id."',concat('".$body->user_id."','-') FROM `mcq_questions` where questions like '%>A.%' ORDER BY rand() LIMIT $total_limit");
			
			//if inserted random questions then return assigned questions
			if($ins_random){
				
				//return 1 to 6 questions on start
				$questions= $this->Common_model->select_fields_where_like_join("assigned_quiz","mcq_questions.questions,mcq_questions.id",[array('table'=>'mcq_questions','condition'=>'mcq_questions.id=assigned_quiz.mcq_questions_id','type'=>'left')],'',false,'','','',array('mcq_questions.id','desc'),array($each_page,$start)); 
				$output['ques_list']=$this->formatQuestions($questions);
				
				//update table for quiz started
				$output['quiz_id']=$this->Online_test_model->quiz_entry($body->user_id,$quiz_time,$total_limit) ;
				return $output;
			}
		}
		else{
			return false;
		}
    } 
	
	//function to format questions (html to text)
	private function formatQuestions($data,$add_history=false)
	{
		if(!empty($data)){
			$return_ques=[]; 
			foreach($data as $each_question){
				
				//format questions (html to text)
				$html = questionAnsFromHtml($each_question->questions,'<p>','</p>');
				if($add_history){
					$return_ques[]=array('quest_id'=>$each_question->id,'question'=>$html['question'],'options'=>$html['options'],'correct_ans'=>strtoupper($each_question->correct_ans),'user_ans'=>$each_question->user_ans?$each_question->user_ans:'Not attempted');
				}else{
					$return_ques[]=array('quest_id'=>$each_question->id,'question'=>$html['question'],'options'=>$html['options']);
				}
			}
			return $return_ques;
		}
		return false;

	}
	
	//save answer of the question
	public function submitAnswer()
	{
		$post = json_decode(file_get_contents("php://input"));
		$body = $post->body; 
		
		//validations
		$this->form_validation->set_data(array('user_id'    =>  $body->user_id  , 'quest_id'    =>  $body->quest_id,'answer'    =>  $body->answer )); 
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required'); 
		$this->form_validation->set_rules('quest_id', 'quest_id', 'trim|required');
		$this->form_validation->set_rules('answer', 'answer', 'trim|required');
		
		//return if validation fails
		if ($this->form_validation->run() == FALSE) { 
			json_response(true,$this->lang->line('field_missing') ,array(),200); 
		}  
		
		//function to save quiz answers
		if ($this->Online_test_model->SaveQuizAnswer($body )){
			json_response(false,$this->lang->line('success'),array(),200);    
        }else{
			json_response(true,$this->lang->line('invalid_request'),array(),200);   
        }  
    }
	
	//function to end quiz 
	public function endQuiz()
	{
		$post = json_decode(file_get_contents("php://input"));
		$body = $post->body;
		
		//validations
		$this->form_validation->set_data(array('user_id'    =>  $body->user_id  )); 
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		
		//return if validation fails
		if ($this->form_validation->run() == FALSE) { 
			json_response(true,$this->lang->line('field_missing'),array(),200); 
		}  
		
		//function to close quiz 
		if ($this->Online_test_model->closeQuiz($body )){
			json_response(false,$this->lang->line('success'),array(),200);    
        }else{
			json_response(true,$this->lang->line('invalid_request'),array(),200);   
        }  
    }
	
	//function to get quiz data
	public function getQuizData()
	{
		$post = json_decode(file_get_contents("php://input"));
		$body = $post->body;
		
		//validations
		$this->form_validation->set_data(array('user_id'    =>  $post->body->user_id )); 
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required'); 
		
		//return if validation fails
		if ($this->form_validation->run() == FALSE) { 
			json_response(true,$this->lang->line('field_missing'),array(),200); 
		}  
		
		//function to get quiz questions
		if ($data_out=$this->getQuizById($body )){
			json_response(false,$this->lang->line('success'),$data_out,200);    
        }else{
			json_response(true,$this->lang->line('invalid_request'),array(),200);   
        }  
    }  
	
	//function to get quiz questions
	private function getQuizById($body)
	 {
		$post = json_decode(file_get_contents("php://input"));
		$body = $post->body;
		$user = $this->Common_model->select_fields_where('users','count(1) as cnt_num',array('id'=>$body->user_id ),1);
		if(isset($user) && $user->cnt_num==1){ 
			$total_limit=$this->total_limit;
			
			//converting into seconds
			$total_time=$this->quiz_time*60;
			$each_page=6;
			$start=0; 
			if(isset($post->body->start)){
				$start=$post->body->start;
			}
			
			//validations
			$this->form_validation->set_data(array('start'    =>  $start )); 
			$this->form_validation->set_rules('start', 'start', "trim|numeric|greater_than[-1]|less_than[$total_limit]");
			
			//return if validation fails
			if ($this->form_validation->run() == FALSE) { 
				json_response(true,$this->lang->line('invalid_request'),array(),200); 
			}    
			
			//return 1 to 6 questions
			$questions= $this->Online_test_model->QuizData($body,array($each_page,$start)); 
			
			//creating response
			$output['ques_list']=$this->formatQuestions($questions,1); 
			$output['total_questions']=$total_limit; 
			$output['total_time']=$total_time; 
			
			//getting quiz data
			$quiz= $this->Common_model->select_fields_where('admit_quizzes','date',array('user_id'    =>  $post->body->user_id  ),1);   
			$end_time=strtotime("+$total_time seconds",strtotime($quiz->date)); 
			
			//end quiz if time ups
			if(($end_time-time())<=0){
				$this->Online_test_model->closeQuiz($body );
				$output['time_remaining']=  0; 
			}else{
				$output['time_remaining']=  $end_time-time() ; 
			}
			
			return $output;
		}else{
			return false;
		}
    } 
	
	//geting quiz result
	public function getQuizList()
	{
		$post = json_decode(file_get_contents("php://input"));
		$body = $post->body;
		
		//validations
		$this->form_validation->set_data(array('user_id'    =>  $post->body->user_id )); 
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required'); 
		
		//return if validation fails
		if ($this->form_validation->run() == FALSE) { 
			json_response(true,$this->lang->line('field_missing'),array(),200); 
		}  
		
		//creating response
		$data['quiz']= $this->Common_model->select_fields_where('admit_quizzes','*',array('user_id'    =>  $post->body->user_id ,'status'    =>  "finished" ),1);   
		$data['questions_list']=$this->Online_test_model->QuizData($body,array($this->total_limit,0));  
		if ($data){
			json_response(false,$this->lang->line('success'),$data,200);    
        }else{
			json_response(true,$this->lang->line('invalid_request'),array(),200);   
        }  
    }  
	
}
