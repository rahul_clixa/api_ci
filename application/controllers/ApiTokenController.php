<?php

defined('BASEPATH') OR exit('No direct script access allowed');


/*
 * This class is used to create token for accessing all api functions
*/

class ApiTokenController extends CI_Controller {

    public function __construct() {
        parent::__construct();
		$this->load->model('Common_model');
    }
	
	//create token
    public function create_token() {
        $post = json_decode(file_get_contents("php://input"));
        $user = $post->cred->user;
        $pass = $post->cred->password;
		
		//check if valid credentials or not
        $token = $this->checkClient($user, $pass);
        if($token != '') {
            json_response(false,$this->lang->line('success'), array('token' => $token), 200);
        } else {
            json_response(true, $this->lang->line('invalid_request'), array(), 200);
        }
    }
	
	//funtion to check credentials 
	private function checkClient($user_id, $user_pw) {
        $token = '';
		
		//checking into DB
        $user = $this->Common_model->select_fields_where('api_clients', '', array('user_id' => $user_id, 'is_active' => 1, 'user_pw' => md5($user_pw)));
        if ($user) {
			//creating random string
            $token = md5(uniqid(rand(), true));
            $this->Common_model->update_query_array('api_clients', array('user_id' => $user_id, 'user_pw' => md5($user_pw)), array('token' => $token));
        }
        return $token;
    }
}
