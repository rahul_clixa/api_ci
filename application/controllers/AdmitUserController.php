<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * This class encapsulates all the functionalities of AdmitUser's  operations. For example, create user.
 * Pleaes be informed this class in not thread safe.
 */
class AdmitUserController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Common_model');
    }
	
	// function calls on requsest to create admit user
    public function createUser() {
        
        $post = json_decode(file_get_contents("php://input"));
        $body = $post->body;
        
		//validations
		$this->form_validation->set_data(array('name' => $post->body->name, 'roll_num' => $post->body->roll_num, 'mobile' => $post->body->mobile, 'email' => $post->body->email, 'admit_card' => $post->body->admit_card));
        $this->form_validation->set_rules('name', 'name', 'trim|required|min_length[3]|max_length[60]');
        $this->form_validation->set_rules('roll_num', 'roll_num', 'trim|required|numeric|min_length[2]|max_length[15]');
        $this->form_validation->set_rules('mobile', 'mobile', 'trim|required|numeric|regex_match[/^[0-9]{10}$/]|is_unique[users.phone]');
        $this->form_validation->set_rules('email', 'email', 'valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('admit_card', 'admit_card', 'trim|required');
        
		//return if validation fails
        if ($this->form_validation->run() == FALSE) {
            json_response(true, $this->lang->line('field_missing'), array(), 200);
        }
        
		//create admit user
        if ($this->createAdmitUser($body)) {
            json_response(false, $this->lang->line('success'), array(), 200);
        } else {
            json_response(true, $this->lang->line('invalid_request'), array(), 200);
        }
    }
	
	//function to save user
    private function createAdmitUser($body,$is_update=0) {
        
        //image checking valid or not then uploading iterator_apply
        $adt_card = base64_decode($body->admit_card);
        
		//if admit card is valid
        if ($img_ext = $this->checkAdmitCard($body->admit_card)) {

			//create folder if doesn't exists
            $file_name = 'admit' . time() . '.' . $img_ext;
            if (!file_exists(FCPATH . '/uploads/')) {
                mkdir(FCPATH . '/uploads/', 0777, true);
            }
            if (!file_exists(FCPATH . '/uploads/admit_card/')) {
                mkdir(FCPATH . '/uploads/admit_card/', 0777, true);
            }
			//convert and save 
            $put_image = file_put_contents(FCPATH . '/uploads/admit_card/' . $file_name, base64_decode($body->admit_card));
            
			
			/* save/update  user  */
			if($is_update==1){
				$user = $this->Common_model->update_query_array('users', array('id'=>$body->user_id),array('roll_num' => $body->roll_num, 'admit_card' => $file_name,'updated_at' => date('Y-m-d H:i:s')));
				if ($user) {
					return true;
				}
			}else{
				
				//random password genertaion
				$password = $this->randomPassword();
				$enc_password = md5($password);
				
				$user = $this->Common_model->insert_record('users', array('username' => $body->name, 'password' => $enc_password, 'email' => $body->email, 'roll_num' => $body->roll_num, 'phone' => $body->mobile, 'admit_card' => $file_name, 'user_rgstn_source_id' => 2, 'created_at' => date('Y-m-d H:i:s')));
				if ($user) {
					//send sms
					send_mobile_message($body->mobile, ucfirst($body->name), $body->mobile, $password);
					
					//send email if exists in request
					if (!empty($body->email)) {

						$this->load->library('parser');
						$mail_data = array(
							'name' => $body->name,
							'user_name' => $body->mobile,
							'password' => $password
						);
						$message = $this->parser->parse('mail_templates/admit_register_email', $mail_data);
						send_email_mg($body->email, 'Bright Tutee - Login Credentials', $message);
					}
					return true;
				}
			}
        }
        return false;
    }

    //create random password
    private function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    //check admit-card valid
    private function checkAdmitCard($base64)
	{
		//if not base64 encoded imaage returns
        if (!base64_decode($base64, true)) {
            return false;
        }
        $img = imagecreatefromstring(base64_decode($base64));
        if (!$img) {
            return false;
        } 
		
		//check valid extensions of image
        $validExtensions = ['png', 'jpeg', 'jpg', 'gif'];
		$info = getimagesizefromstring(base64_decode($base64));
        $img_ext = explode('/', $info['mime']);
        if (!in_array($img_ext[1], $validExtensions)) {
            return false;
        }
        return $img_ext[1];
    }
	
	// function calls on requsest to update existing user
    public function UpdateUser() {
        
        $post = json_decode(file_get_contents("php://input"));
        $body = $post->body;
        
		//validations
		$this->form_validation->set_data(array('roll_num' => $post->body->roll_num, 'admit_card' => $post->body->admit_card,'user_id'    =>  $post->body->user_id )); 
        $this->form_validation->set_rules('roll_num', 'roll_num', 'trim|required|numeric|min_length[2]|max_length[15]');
        $this->form_validation->set_rules('admit_card', 'admit_card', 'trim|required');
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
        
		//return if validation fails
        if ($this->form_validation->run() == FALSE) {
            json_response(true, $this->lang->line('field_missing'), array(), 200);
        }
        
		//create admit user
        if ($this->createAdmitUser($body,1)) {
            json_response(false, $this->lang->line('success'), array(), 200);
        } else {
            json_response(true, $this->lang->line('invalid_request'), array(), 200);
        }
    }
	
}
