<?php
 
/* mobile apis */

//admit controller routes for mobile
Route::group('bottle', ['middleware' => ['CAuthMiddleware']], function(){
	Route::post('register-user', 'AdmitUserController@createUser');
	Route::post('update-user', 'AdmitUserController@UpdateUser');
	Route::post('admit-start-test', 'OnlineTestController@startAdOnlineTest');
	Route::post('admit-submit-answer', 'OnlineTestController@submitAnswer');
	Route::post('admit-submit-quiz', 'OnlineTestController@endQuiz');
	Route::post('admit-quiz-data', 'OnlineTestController@getQuizData');
	Route::post('admit-quiz-list', 'OnlineTestController@getQuizList');
});

 