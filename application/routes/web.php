<?php
/* web clients  */


//default page 
Route::get('/','Welcome@index');

//overriding 404 page
Route::set('404_override', function(){
   json_response(true,'Invalid request',array(),400);   
});

Route::set('translate_uri_dashes',FALSE);

//for global
// Route::middleware('CAuthMiddleware', 'pre_controller');

//create connection
Route::post('createConnection', 'ApiTokenController@create_token');

//admit controller routes
Route::group('hash', ['middleware' => ['CAuthMiddleware']], function(){
	Route::post('register-user', 'AdmitUserController@createUser');
	Route::post('update-user', 'AdmitUserController@UpdateUser');
	Route::post('admit-start-test', 'OnlineTestController@startAdOnlineTest');
	Route::post('admit-submit-answer', 'OnlineTestController@submitAnswer');
	Route::post('admit-submit-quiz', 'OnlineTestController@endQuiz');
	Route::post('admit-quiz-data', 'OnlineTestController@getQuizData');
	Route::post('admit-quiz-list', 'OnlineTestController@getQuizList');
});