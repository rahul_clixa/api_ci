<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * This class is used to access all data from database for admit card test operartions
 */

class Online_test_model extends CI_Model
{
	//create quiz 
	public function quiz_entry($user_id,$quiz_time,$quiz_entry)
	{
		//check if already quiz in progress
		$this->db->select(['date','id']);
		$this->db->where(['user_id' => $user_id]);
		$this->db->order_by('id','desc');
        $data = $this->db->get('admit_quizzes');
		$res= $data->row(); 
		// $last_date=$res->date;
		// $new_date=date('Y-m-d H:i:s',strtotime("+$quiz_time minutes",strtotime($last_date)));
		$current_time=date('Y-m-d H:i:s',time()); 
		if ( $data->num_rows()== 0 )
        {
			$this->db->insert('admit_quizzes', array('user_id' => $user_id,'date'=> date('Y-m-d H:i:s'),'status'    => 'ongoing','total_ques'    =>$quiz_entry) );
			return $this->db->insert_id();
		}else{
			return $res->id;
		} 
	}
	
	//save quiz answers
	public function SaveQuizAnswer($data)
	{
		//get quiz id
		$this->db->select(['id','status']);
		$this->db->where(['user_id' =>  $data->user_id]);
        $quiz_id = $this->db->get('admit_quizzes')->row(); 
		
		//return if already quiz ended
		if($quiz_id->status=='finished'){
			return false;
		}
		
		//get quiz history
		$this->db->where(['user_id'=> $data->user_id ,'admit_quizzes_id'=> $quiz_id->id ,'mcq_questions_id'=> $data->quest_id]);
        $q = $this->db->get('admit_quiz_history');
		
        if ( $q->num_rows() > 0 )
        {
            $this->db->where(['user_id'=> $data->user_id ,'admit_quizzes_id'=> $quiz_id->id ,'mcq_questions_id'=> $data->quest_id]); 
            if($this->db->update('admit_quiz_history',['answer'=>$data->answer])){ 
                return TRUE;
            } 
        } else { 
            $this->db->insert('admit_quiz_history',['created_on'=> date('Y-m-d H:i:s'),'user_id'=> $data->user_id  ,'admit_quizzes_id'=> $quiz_id->id ,'mcq_questions_id'=> $data->quest_id,'answer'=> $data->answer]);
            $insertedID = $this->db->insert_id();
            if($insertedID > 0){
                return TRUE;
            } 
        }
		 return FALSE;
	}
	
	//end quiz
	public function closeQuiz($data)
	{
		//check if already closed
		$this->db->where(['user_id'=> $data->user_id]);
        $check_quiz = $this->db->get('admit_quizzes');
		$check_quiz_data=$check_quiz->row(); 
		
		//check if not already finished
		if($check_quiz_data->status!='finished'){
			//get number of attempted questions
			$this->db->where(['user_id'=> $data->user_id]);
			$attem_qr = $this->db->get('admit_quiz_history');
			$attempted_quest=$attem_qr->num_rows(); 
			
			//marks of each question
			$this->db->select('mcq_questions.id,correct_marks,wrong_marks')->from('mcq_questions');
			$this->db->where('`mcq_questions.id` IN (SELECT mcq_questions_id FROM `assigned_quiz` where users_id='.$data->user_id.')'); 
			$this->db->join('mcq','mcq.id=mcq_questions.mcq_id','left');
			$marks_arr = $this->db->get()->result_array();
			 
			//correct answers && wrong answers list
			$this->db->select('mcq_questions.id,case when answer=option_uid then "1" else "0" end as result ', FALSE)->from('admit_quiz_history'); 
			$this->db->join('mcq_questions','mcq_questions.id=admit_quiz_history.mcq_questions_id','left');
			$this->db->join('mcq_answers','mcq_answers.questions_uid=mcq_questions.questions_uid','left');
			$this->db->where(['admit_quiz_history.user_id'=> $data->user_id,'admit_quiz_history.admit_quizzes_id'=> $check_quiz_data->id]);
			$res_match_arr = $this->db->get()->result_array();
			
			
			//calculating result
			$percentage=0;
			$correct=0;
			$wrong=0; 
			$lefts=0;
			$marks=0;
			$max_marks=0;
			$status='finished';
			$attempted_ans_res=array();
			if($res_match_arr){
				foreach($res_match_arr as $res_match){
					$attempted_ans_res[ $res_match['id']]=$res_match['result'];
				}
			}
			if($marks_arr){
				foreach($marks_arr as $marks_data){
					if(isset($attempted_ans_res[$marks_data['id']])){
						
						//if correct answer
						if($attempted_ans_res[$marks_data['id']]==1){
							$marks+=$marks_data['correct_marks'];
							$correct++;
						}else{
							$marks-=$marks_data['wrong_marks'];
							$wrong++;
						}
					}else{
						$marks-=$marks_data['wrong_marks']; 
					}
				$max_marks+=$marks_data['correct_marks'];
				}
			}
			$percentage=number_format((float)($marks/$max_marks)*100, 2, '.', '');
			if($percentage<0){
				$percentage=0;
			}
			
			//update status and end quiz
			$this->db->where(['id'=> $check_quiz_data->id]); 
			$this->db->set('lefts', 'total_ques-'. $attempted_quest.'',false);
			if($this->db->update('admit_quizzes',['percentage'=>$percentage,'correct'=>$correct,'wrong'=>$wrong, 'marks'=>$marks,'status'=>$status,'attempt'=>$attempted_quest])){  
				return TRUE;
			}
		} 
		return FALSE; 
		
	}
	
	//get result data and questions
	public function QuizData($data,$limit){
		//get list of questions
		$this->db->select('`mcqQ`.`questions`, `mcqQ`.`id`,(select case when status="finished" then mcq_answers.option_uid else "" end from assigned_quiz join mcq_questions on mcq_questions.id=assigned_quiz.mcq_questions_id join mcq_answers on mcq_answers.questions_uid=mcq_questions.questions_uid join admit_quizzes on admit_quizzes.user_id=assigned_quiz.users_id where assigned_quiz.users_id="'.$data->user_id.'" and mcq_questions_id=mcqQ.id) as correct_ans,(select answer from admit_quiz_history where admit_quiz_history.user_id="'.$data->user_id.'" and mcq_questions_id=mcqQ.id ) as user_ans ', FALSE)->from('assigned_quiz'); 
		$this->db->join('mcq_questions mcqQ','mcqQ.`id`=`assigned_quiz`.`mcq_questions_id`','left');
		$this->db->join('mcq_answers','mcq_answers.questions_uid=mcqQ.questions_uid','left'); 
		$this->db->where(['assigned_quiz.users_id'=> $data->user_id]);
		$this->db->order_by('mcqQ.id','desc');
		$this->db->limit($limit[0],$limit[1]);
		$res_match_arr = $this->db->get()->result(); 
		return $res_match_arr;
	}
}