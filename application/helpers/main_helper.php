<?php 
require APPPATH . 'third_party/mailgun/vendor/autoload.php';

use Mailgun\Mailgun;

 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* json response */
if ( ! function_exists('json_response'))
{
    function json_response($error=false,$message,$body,$resp_c=200)
    {
        header("Content-Type: application/json");
		$data=array('error'=>$error,'message'=>$message,'body'=>$body);
		$json = json_encode($data);
		http_response_code($resp_c);
		echo $json;
		exit;
    }   
}
/* sends message to mobile  */
if ( ! function_exists('send_mobile_message'))
{
    function send_mobile_message($mobile,$VAR1,$VAR2,$VAR3)
    {
		$curl = curl_init();
		$CI = get_instance(); 
		
		/* template
			// Hi #VAR1#, Your Login user ID is #VAR2# and password is  #VAR3#.  
		*/
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://2factor.in/API/V1/".$CI->config->item('sms_api')."/ADDON_SERVICES/SEND/TSMS/",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "TemplateName=".$CI->config->item('sms_api_template')."&From=".$CI->config->item('sms_api_from')."&To=$mobile&VAR1=$VAR1&VAR2=$VAR2&VAR3=$VAR3",
		  CURLOPT_HTTPHEADER => array(
			"content-type: application/x-www-form-urlencoded"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl); 
		if ($err) {
		  // echo "cURL Error #:" . $err;
		  return false;
		} else {
		  return $response;
		}
	}   
}
/* sends emails */
if ( ! function_exists('send_email_mg'))
{
    function send_email_mg($to_email,$subject,$message,$cc='',$bcc='',$attachment=array())
    {	
		$CI = get_instance();  
		$mgClient = new Mailgun($CI->config->item('mail_api'));
		$domain = $CI->config->item('mail_domain');
		$mail_arr=array(
			'from' => $CI->config->item('mail_from_name').' <'.$CI->config->item('mail_from_email').'>',
			'to' => $to_email, 
			'subject' => $subject,
			'html' => $message
		);
		if(!empty($cc)){
			$mail_arr['cc']=$cc;
		}
		if(!empty($bcc)){
			$mail_arr['bcc']=$bcc;
		}
		$result = $mgClient->sendMessage($domain,$mail_arr);
		if (!$result) { 
		  return false;
		} else {
		  return $result;
		}
	}   
}
/* format questions html */
if ( ! function_exists('str_between_tags'))
{
    function questionAnsFromHtml($string, $start, $end)
	{
		$result = array();
		$string = " ".$string;
		$offset = 0;
		$iter=0;
		$result['question']='';
		while(true)
		{
			$ini = strpos($string,$start,$offset);
			if ($ini == 0)
				break;
			$ini += strlen($start);
			$len = strpos($string,$end,$ini) - $ini;
			
			//check iterations to skip for options
			$chk_string=explode('>A.',$string);
			$cnt_p_tag=substr_count($chk_string[0], '</p>'); 
			
			if(!empty(str_replace('&nbsp;','',strip_tags(substr($string,$ini,$len))))){
				if($iter<$cnt_p_tag){
					// $result['question'] = str_replace('&nbsp;','',strip_tags(substr($string,$ini,$len)));
					$result['question'].=  substr($string,$ini,$len);
				}else{
					$result['options'][] =  substr($string,$ini,$len);
					// $result['options'][] = str_replace('&nbsp;','',strip_tags(substr($string,$ini,$len)));
				}
			}
			$offset = $ini+$len;
			$iter++;
		}
		return $result;
	} 
}